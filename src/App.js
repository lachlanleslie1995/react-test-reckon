import React from "react";
import { render } from "react-dom";
import Results from "./results";

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Reckon test app.</h1>
        <Results />
      </div>
    );
  }
}

render(<App />, document.getElementById("root"));
