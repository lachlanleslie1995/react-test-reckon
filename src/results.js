import React from "react";

class Results extends React.Component {
  state = {
    error: null,
    isLoaded: false,
    range: "",
    divisors: ""
  };

  componentDidMount() {
    //failed to get around cross origin resource sharing security protocols on chrome as I was working on localhost setup
    let range = fetch("https://join.reckon.com/test1/rangeInfo");
    let divisors = fetch("https://join.reckon.com/test1/divisorInfo");
    Promise.all([range, divisors])
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            range: result.range,
            divisors: result.divisors
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    // test data because requests were blocked by cross origin sharing
    // let lower = 0;
    // let upper = 100;

    // let divisors = [
    //   {
    //     divisor: 3,
    //     output: "boss"
    //   },
    //   {
    //     divisor: 5,
    //     output: "hogg"
    //   }
    // ];

    let resultsArray = [];

    let lower = this.state.range.lower;
    let upper = this.state.range.upper;
    let divisors = this.state.divisors;

    for (let i = lower; i <= upper; i++) {
      resultsArray[i] = i + ": ";
      divisors.forEach(details => {
        if (i != 0 && i % details.divisor === 0) {
          resultsArray[i] += details.output;
        }
      });
    }

    return (
      <div>
        {resultsArray.map((result, i) => {
          return <div key={i}>{result}</div>;
        })}
      </div>
    );
  }
}

export default Results;
